'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const path = require('path')
const mongoose = require('mongoose')

app.set('view engine', 'ejs')
app.use(express.static(path.join(__dirname, 'public')))
const port = process.env.PORT || 3000

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

mongoose.Promise = global.Promise
// setup mongoose
app.db = mongoose.createConnection('mongodb://localhost/news')
app.db.on('error', console.error.bind(console, 'mongoose connection error: '));
// config data models
require('./models')(app, mongoose)
// setup routes
require('./router')(app)

app.listen(port, () => {
  console.log(`API REST corriendo en http://localhost:${port}`)
})
